console.log("==========Looping pertama==========")
var i = 1
while(i < 21) {
    if (i % 2 == 0) {
        console.log(i+' I love Coding');    
    } 
  i++;
}

console.log("==========Looping Kedua==========")
var x = 20
while (x>0) {
    if (x%2==0){
        console.log(x+" i will become a mobile developer")
    }   
    x--;     
}

console.log("==========No 2==========")
for(var a = 1; a < 21; a++) {
    if(a % 2 == 1 && a % 3 !=0){
        console.log(a+" Santai");
    }
    if (a % 2 == 0){
        console.log(a+" Berkualtias");
    }
    if(a%2==1&&a%3==0){
        console.log(a+" I Love Coding");
    }
  }

console.log("==========No 3==========")
var kosong = ''
for (var baris = 1; baris <= 4; baris++){
    for(var kolom = 1; kolom <=8; kolom++){
        kosong+="#";
    }
    kosong+='\n';
}
console.log(kosong)

console.log("==========No 4==========")
var kosongg = ''
for (var baris = 1; baris <= 7; baris++){
    for(var kolom = 1; kolom <=baris; kolom++){
        kosongg+="#";
    }
    kosongg+='\n';
}
console.log(kosongg)

console.log("==========No 5==========")
var kosonggg = ''
for (var baris = 1; baris <= 8; baris++){
    for(var kolom = 1; kolom <=8; kolom++){
        if (baris%2==1){
            if(kolom%2==0){
                kosonggg+="#"
            }
            else{
                kosonggg+=" "
            }
        }
        else{
            if(kolom%2==0){
                kosonggg+=" "
            }
            else{
                kosonggg+="#"
            }
        }
    }
    kosonggg+='\n';
}
console.log(kosonggg)