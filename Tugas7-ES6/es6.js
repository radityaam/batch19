console.log("========== NO 1 ==========")
goldenFunction = () => {
    console.log("This is Golden!!!")
}
goldenFunction()

console.log("========== NO 2 ==========")
literal = (firstName,lastName) => {
    return fullName = {firstName,lastName}
}
console.log(literal("william","imoh"))

console.log("========== No 3 ==========")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const {firstName,lastName,destination,occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation,spell)

console.log("========== NO 4 ==========")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]

console.log(combined)

console.log("========== NO 5 ==========")
const planet = "earth"
const view = "glass"
const lorem = 'Lorem '
const dolor = 'dolor sit amet, '
const ad = ' ad minim veniam'
const consectetur = 'consectetur adipiscing elit,'
var before = `${lorem} ${view} ${dolor} ${consectetur} ${planet} ${ad}`

console.log(before) 